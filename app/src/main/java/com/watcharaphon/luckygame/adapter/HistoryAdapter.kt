package com.watcharaphon.luckygame.adapter

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.luckygame.data.dataSoure
import com.watcharaphon.luckygame.database.card.card
import com.watcharaphon.luckygame.databinding.ListHistoryBinding

class HistoryAdapter(private val onItemClicked: (card) -> Unit) : ListAdapter<card,HistoryAdapter.HistoryViewHolder>(DiffCallback) {
    class HistoryViewHolder(private var binding: ListHistoryBinding): RecyclerView.ViewHolder(binding.root) {
        fun bind(card: card) {
            binding.itemImageCard.setImageResource(dataSoure.loadImage[card.cardId].imageResourceIdReward)
            binding.nameCard.text = card.nameCard
            binding.dateTime.text = card.timestamp
            Log.d(TAG, "bind: ${card.cardId}")
        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HistoryViewHolder {
        val viewHolder = HistoryViewHolder(
            ListHistoryBinding.inflate(
                LayoutInflater.from( parent.context),
                parent,
                false
            )
        )
        viewHolder.itemView.setOnClickListener {
            val position = viewHolder.adapterPosition
            onItemClicked(getItem(position))
        }
        return viewHolder
    }
    override fun onBindViewHolder(holder: HistoryViewHolder, position: Int) {
        val current = getItem(position)
        holder.itemView.setOnClickListener {
            onItemClicked(current)
        }
        holder.bind(current)
    }

    companion object {
        private val DiffCallback = object : DiffUtil.ItemCallback<card>() {
            override fun areItemsTheSame(oldItem: card, newItem: card): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: card, newItem: card): Boolean {
                return oldItem == newItem
            }
        }
    }
}
package com.watcharaphon.luckygame.adapter

import android.os.Handler
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.luckygame.SelectCardsFragmentDirections
import com.watcharaphon.luckygame.data.dataSoure
import com.watcharaphon.luckygame.databinding.BackCardsBinding

class CardAdapter() :
    RecyclerView.Adapter<CardAdapter.CardViewHolder>() {
    class CardViewHolder(private var binding: BackCardsBinding) :
        RecyclerView.ViewHolder(binding.root) {
        val Image = binding.itemImageCard
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CardViewHolder {
        val viewHolder = CardViewHolder(
            BackCardsBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
        return viewHolder
    }

    override fun getItemCount(): Int = dataSoure.loadImage.size

    override fun onBindViewHolder(holder: CardViewHolder, position: Int) {
        val card = dataSoure.loadImage[position]
        holder.Image.setImageResource(card.imageResourceIdBack)

        var r = (0..21).random()

        holder.Image.setOnClickListener() {
            holder.Image.setImageResource(dataSoure.loadImage[r].imageResourceIdReward)
            var action =
                SelectCardsFragmentDirections.actionSelectCardFragmentToResultFragment(r)
            Handler().postDelayed({
                holder.Image.findNavController().navigate(action)
            }, 1500)
        }
    }
}
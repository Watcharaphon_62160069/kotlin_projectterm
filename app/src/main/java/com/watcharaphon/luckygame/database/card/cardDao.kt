package com.watcharaphon.luckygame.database.card

import androidx.room.*
import kotlinx.coroutines.flow.Flow

@Dao
interface cardDao {
    @Query("SELECT * from card ORDER BY timeStamp ASC")
    fun getAll(): Flow<List<card>>
    
    @Insert
    suspend fun insert(card: card)

    @Update
    suspend fun update(card: card)

    @Delete
    suspend fun delete(card: card)


}
package com.watcharaphon.luckygame.database.card

import androidx.annotation.NonNull
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class card(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    @ColumnInfo(name = "nameCard") val nameCard: String,
    @ColumnInfo(name = "timeStamp") val timestamp: String,
    @ColumnInfo(name = "cardId") val cardId: Int
)
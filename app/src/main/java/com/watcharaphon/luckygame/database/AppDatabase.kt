package com.watcharaphon.luckygame.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.watcharaphon.luckygame.database.card.card
import com.watcharaphon.luckygame.database.card.cardDao

@Database(entities = [card::class], version = 1, exportSchema = false)
abstract class AppDatabase :RoomDatabase(){

    abstract fun cardDao(): cardDao

    companion object{
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "HistoryCardDatabase")
                    .fallbackToDestructiveMigration()
                    .build()
                INSTANCE = instance
                return instance
            }
        }
    }
}
package com.watcharaphon.luckygame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.watcharaphon.luckygame.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _building: FragmentHomeBinding? = null
    private val building get() = _building

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _building = FragmentHomeBinding.inflate(inflater, container,false)


        return building?.root
    }
    override fun onDestroyView() {
        _building = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        building?.screen?.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToSelectCardFragment()
            view.findNavController().navigate(action)
        }

        building?.imageView2?.setOnClickListener {
            val action = HomeFragmentDirections.actionHomeFragmentToHistoryFragment()
            view.findNavController().navigate(action)
        }
    }
}
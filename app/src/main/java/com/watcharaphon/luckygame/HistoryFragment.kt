package com.watcharaphon.luckygame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.luckygame.adapter.HistoryAdapter
import com.watcharaphon.luckygame.databinding.FragmentHistoryBinding
import com.watcharaphon.luckygame.viewModel.CardViewModel
import com.watcharaphon.luckygame.viewModel.CardViewModelFactory

class HistoryFragment : Fragment() {

    private val viewModel: CardViewModel by activityViewModels {
        CardViewModelFactory(
            (activity?.application as CardApplication).database.cardDao()
        )
    }
    private var _binding: FragmentHistoryBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {

        _binding = FragmentHistoryBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.layoutManager = LinearLayoutManager(this.context)
        val historyAdapter = HistoryAdapter{}
        binding.recyclerView.adapter = historyAdapter
        viewModel.allHistory.observe(this.viewLifecycleOwner){item ->
            item.let {
                historyAdapter.submitList(it)
            }
        }

    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
package com.watcharaphon.luckygame.viewModel

import android.media.CamcorderProfile.getAll
import androidx.lifecycle.*
import com.watcharaphon.luckygame.database.card.card
import com.watcharaphon.luckygame.database.card.cardDao
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

class CardViewModel(private val cardDao: cardDao) : ViewModel() {

    val allHistory: LiveData<List<card>> = cardDao.getAll().asLiveData()

    private fun insertCard(card:card){
        viewModelScope.launch{
            cardDao.insert(card)
        }
    }

    private fun getNewCard(nameCard:String,timestamp:String,cardId:Int): card {
        return card(nameCard = nameCard, timestamp = timestamp, cardId = cardId)
    }

    fun addNewCard(nameCard:String,timestamp:String,cardId:Int){
        val newCard = getNewCard(nameCard,timestamp,cardId)
        insertCard(newCard)
    }
}

class CardViewModelFactory(
    private val cardDao: cardDao
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(CardViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return CardViewModel(cardDao) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}

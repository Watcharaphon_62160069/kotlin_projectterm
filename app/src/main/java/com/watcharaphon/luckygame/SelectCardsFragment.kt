package com.watcharaphon.luckygame

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.watcharaphon.luckygame.adapter.CardAdapter
import com.watcharaphon.luckygame.database.card.card
import com.watcharaphon.luckygame.databinding.FragmentSelectCardsBinding
import com.watcharaphon.luckygame.viewModel.CardViewModel
import com.watcharaphon.luckygame.viewModel.CardViewModelFactory

class SelectCardsFragment : Fragment() {

    private var _binding: FragmentSelectCardsBinding? = null
    private val binding get() = _binding!!
    private lateinit var recyclerView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentSelectCardsBinding.inflate(inflater, container,false)
        return binding?.root
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        recyclerView = binding.RecycleView
        var cardAdapter = CardAdapter()
        recyclerView.setHasFixedSize(true)
        recyclerView.adapter = cardAdapter

    }
}
package com.watcharaphon.luckygame

import android.app.Application
import com.watcharaphon.luckygame.database.AppDatabase

class CardApplication: Application(){
    val database: AppDatabase by lazy {
        AppDatabase.getDatabase(this)
    }
}
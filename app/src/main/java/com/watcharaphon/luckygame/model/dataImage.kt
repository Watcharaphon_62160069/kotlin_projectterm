package com.watcharaphon.luckygame.model

import android.accounts.AuthenticatorDescription
import androidx.annotation.DrawableRes

data class dataImage(
    @DrawableRes val imageResourceIdBack: Int,
    @DrawableRes val imageResourceIdReward: Int,
    val nameCard: String,
    val descriptionCard: String,
    val link:String,
    val statue: Int)

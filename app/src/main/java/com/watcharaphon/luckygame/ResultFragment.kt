package com.watcharaphon.luckygame

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.navArgs
import com.watcharaphon.luckygame.data.dataSoure
import com.watcharaphon.luckygame.databinding.FragmentResultBinding
import com.watcharaphon.luckygame.viewModel.CardViewModel
import com.watcharaphon.luckygame.viewModel.CardViewModelFactory
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class ResultFragment : Fragment() {

    private var _binding: FragmentResultBinding? = null
    private val binding get() = _binding
    val args: ResultFragmentArgs by navArgs()

    private val viewModel: CardViewModel by activityViewModels {
        CardViewModelFactory(
            (activity?.application as CardApplication).database
                .cardDao()
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        addNewCard()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentResultBinding.inflate(inflater, container,false)
        return binding?.root
    }
    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding?.exit?.setOnClickListener {
            val action = ResultFragmentDirections.actionResultFragmentToHomeFragment()
            view.findNavController().navigate(action)
        }
        binding?.imageCard?.setImageResource(dataSoure.loadImage[args.idCard].imageResourceIdReward)
        binding?.NameCard?.text = dataSoure.loadImage[args.idCard].nameCard
        binding?.meanCard?.text = dataSoure.loadImage[args.idCard].descriptionCard
        binding?.reading?.setOnClickListener{
            val queryUrl: Uri = Uri.parse("${SEARCH_PREFIX}${dataSoure.loadImage[args.idCard].link}")
            val intent = Intent(Intent.ACTION_VIEW, queryUrl)
            context?.startActivity(intent)
        }
    }

    private fun addNewCard(){
        val currentTime = LocalDateTime.now()
        val fomatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)

        val fomatterd = currentTime.format(fomatter)
        viewModel.addNewCard(
            dataSoure.loadImage[args.idCard].nameCard,
            fomatterd.toString(),
            args.idCard
        )
    }
    companion object {
        const val SEARCH_PREFIX = "https://www.shitsuren-tarot.com/tarot/"
    }
}
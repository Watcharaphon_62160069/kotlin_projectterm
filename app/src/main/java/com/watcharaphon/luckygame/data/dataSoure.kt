package com.watcharaphon.luckygame.data

import com.watcharaphon.luckygame.R
import com.watcharaphon.luckygame.model.dataImage

object dataSoure {
    val loadImage: List<dataImage> =
        listOf(
            dataImage(
                R.drawable.card_back,
                R.drawable.m_0,
                "THE FOOL",
                "เป็นไพ่แห่งการผจญภัย อิสระ และพร้อมที่จะเดินหน้าแม้ข้างหน้าจะเป็นอันตราย",
                "the-fool/",
                0
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_1,
                "THE MAGICIAN",
                "ไพ่ใบนี้บ่งบอกลักษณะของคนที่มีความรู้ ความสามารถ ฉลาด เข้าใจโลก เป็นนักวิชาการ นักวิจัย  แต่ในอีกมุมนึงก็อาจจะดูเป็นคนที่พูดดี พูดเก่ง มีความรู้ มากเกินกว่าที่จะเป็นปฏิบัติจริง",
                "the-magician/",
                1
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_2,
                "THE HIGH PRIESTESS",
                "ไพ่ใบนี้บ่งบอกถึงพลังลึกลับซ่อนเร้น จิตวิญญาณ สัญชาตญาณ ความคิดที่ซ่อนเร้น ความซับซ้อน ไม่ชัดเจน เหมือนพระจันทร์ที่ส่องแสงสลัวตอนกลางคืน ราชินีพระจันทร์ ผู้มีความไร้เดียงสาบริสุทธิ์ในตัว ดูภายนอกก็ดูตรงๆ แต่ภายในกลับลึกลับ ซับซ้อน ดูน่าค้นหา",
                "the-high-priestess/",
                2
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_3,
                "THE EMPRESS",
                "ไพ่ใบนี้มีความหมายถึงความอุดมสมบูรณ์ ธรรมชาติ ความสวยงาม ความเป็นแม่ และ การให้กำเนิด การมีลูก",
                "the-emperor/",
                3
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_4,
                "THE EMPEROR",
                "ไพ่ใบนี้มีความหมายถึง ความมั่นคง ความควบคุม ความนิ่ง ความเป็นพ่อ ที่เข้มแข็ง การดูแลปกป้อง",
                "tarot/the-emperor/",
                4
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_5,
                "THE HIEROPHANT",
                "ไพ่ใบนี้มีความหมายถึง จารีต ประเพณี ศาสนา พิธีกรรม การสั่งสอนเผยแพร่",
                "the-hierophant/",
                5
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_6,
                "THE LOVERS",
                "ไพ่ใบนี้มีความหมายถึง ความรัก พรหมลิขิต การพบเจอ ความเข้ากันได้ โอกาสที่เข้ามา ทางเลือก",
                "the-lovers/",
                6
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_7,
                "THE CHARIOT",
                "ไพ่ใบนี้มีความหมายถึง ความมุ่งมั่น แน่วแน่ ความมุทะลุ และความสามารถในการควบคุม การเดินทาง",
                "the-chariot/",
                7
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_8,
                "STRENGTH",
                "ไพ่ใบนี้มีความหมายถึง ความเข้มแข็ง ความอดทน  การควบคุมจิตใจ",
                "strength/",
                8
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_9,
                "THE HERMIT",
                "ไพ่ใบนี้มีความหมายถึง การปลีกวิเวก การแยกตัว การค้นหาตัวเอง ดูจิตใจตัวเอง การแยกตัวเพื่อมองสถานการณ์ในมุมกว้าง",
                "the-hermit/",
                9
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_10,
                "WHEEL OF FORTUNE",
                "ไพ่ใบนี้มีความหมายถึง โชคชะตาที่เปลี่ยนแปลง การเปลี่ยนแปลงที่ไม่สามารถควบคุมได้ ความโชคดี",
                "wheel-of-fortune/",
                10
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_11,
                "JUSTICE",
                "ไพ่ใบนี้มีความหมายถึง ความยุติธรรม ความเท่าเทียม การปรับสมดุลให้เท่าเทียม การตัดสินใจอย่างยุติธรรม กฏหมาย",
                "justice/",
                11
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_12,
                "THE HANGED MAN",
                "ไพ่ใบนี้มีความหมายถึง การเสียสละ การพัก ชั่วคราวเพื่อมองมุมใหม่ การตระหนักรู้",
                "the-hanged-man/",
                12
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_13,
                "DEATH",
                "ไพ่ใบนี้มีความหมายถึง การจบสิ้นเพื่อเริ่มต้นใหม่ การเปลี่ยนแปลงสภาพ",
                "death/",
                13
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_14,
                "TEMPERANCE",
                "ไพ่ใบนี้มีความหมายถึง การหาสมดุล การปรับตัว การทดลอง และความอดทน",
                "temperance/",
                14
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_15,
                "THE DEVIL",
                "ไพ่ใบนี้มีความหมายถึง การผูกมัด ความหลงใหล การเสพติด เซ็กส์ หรือกรรมที่ทำให้มีการผูกมัด",
                "the-devil/",
                15
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_16,
                "THE TOWER",
                "ไพ่ใบนี้มีความหมายถึง การเปลี่ยนแปลงกระทันหัน เปลี่ยนแปลงอย่างรุนแรง การพังทลาย",
                "the-tower/",
                16
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_17,
                "THE STAR",
                "ไพ่ใบนี้มีความหมายถึง ความหวัง การฟื้นฟูรักษา การเริ่มต้นใหม่",
                "the-star/",
                17
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_18,
                "THE MOON",
                "ไพ่ใบนี้มีความหมายถึง ความกังวล ความกลัว สิ่งที่กลัวเก็บซ่อนไว้จะถูกเปิดเผย อารมณ์อ่อนไหว ความรู้สึกไม่ปลอดภัย",
                "the-moon/",
                18
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_19,
                "THE SUN",
                "ไพ่ใบนี้มีความหมายถึง ความสดใส ความสุข ความสำเร็จ สิ่งดีๆที่กำลังเข้ามา สิ่งที่กำลังเปิดเผยชัดเจน ความขุ่นมัวที่หายไป",
                "the-sun/",
                19
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_20,
                "JUDGEMENT",
                "ไพ่ใบนี้มีความหมายถึง การตัดสิน ผลของกรรม ผลของการกระทำ สิ่งที่ถูกปิดไว้จะถูกเปิดเผย",
                "judgement/",
                20
            ),
            dataImage(
                R.drawable.card_back,
                R.drawable.m_21,
                "THE WORLD",
                "ไพ่ใบนี้มีความหมายถึง ความสมบูรณ์ การจบสิ้น ความสุข ความมีอิสระ",
                "the-world/",
                21
            )
        )
}
